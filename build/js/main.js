// var istouch = ( !!('ontouchstart' in window)) ? 'touchstart' : 'click';

$(function(){
    var map;
    var locationMAP;

    $('.carousel').slick({
        infinite: true,
        slidesToShow: 1,
        autoplay: true,
        // centerPadding: '20%',
        variableWidth: true,
        centerMode: true,
        slidesToScroll: 1,
        prevArrow: '.carousel-prev',
        nextArrow: '.carousel-next'
    });

    $(document).on('click','.slick-slide', function () {
        if ($(this).next().hasClass('slick-active')) {
            $('.carousel-news').slick('slickPrev');
            $('.carousel').slick('slickPrev');
        };

        $('.carousel-news').slick('slickNext');
        $('.carousel').slick('slickNext');
    })

    $('.carousel-news').slick({
        infinite: true,
        slidesToShow: 1,
        autoplay: true,
        // centerPadding: '0%',
        // centerMode: true,
        slidesToScroll: 1,
        prevArrow: '.carousel-prev',
        nextArrow: '.carousel-next'
    });

    // function alignText () {
    //     $(".slick-center")
    //         .css("text-align", "center");

    //     $(".slick-center")
    //         .prev()
    //         .css("text-align", "right");

    //     $(".slick-center")
    //         .next()
    //         .css("text-align", "left");
    // }

    // $('.carousel').on('afterChange', function(event, slick, direction){
    //     alignText();
    // });

    // alignText();

    $(".carousel-wrap").show();

    $(".collapse-button").hover(
        function() {
            $('.hidden-like').show(1000);
        }, function() {
            $('.hidden-like').hide();
        }
    );

    $(".hidden-like").hover(
        function() {
            $(this).show();
        }, function() {
            $(this).hide(1000);
        }
    );

    if($("#map").length){
         locationMAP = [
            45.012810,
            39.051505
        ];
        DG.then(function () {
            map = DG.map('map', {
                center: locationMAP,
                zoom: 18
            });

            DG.marker(locationMAP).addTo(map);
        });
    }
});
